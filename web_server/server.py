import os
from flask import Flask, request

app = Flask(__name__)

@app.route("/")
def home():
    return "Hello World!"


@app.route('/eval_messages')
def query_example():
    recruiter_message = request.args.get('recruiter_message')
    candidate_message = request.args.get('candidate_message')
    # A MODEL PROCESSING RECRUIER MESSAGE AND CANDIDATE MESSAGE AND RETURN PREDICTION SHOULD LAY HERE ...
    return "<h1>The recruiter_message is: {}</h1>\n<h1>The recruiter_message is: {}</h1>\n<h1>Category of message is {}<h1>".format(
        recruiter_message,
        candidate_message, 1)


if __name__ == "__main__":
    app.secret_key = os.urandom(12)
    app.run(host='0.0.0.0', port=4555, debug=True)
