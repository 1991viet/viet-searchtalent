import os
import random
import string
from flask import Flask, request, render_template, send_from_directory, flash, session, redirect, url_for
from werkzeug.utils import secure_filename
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from src.utils import get_location, get_skills, retrieve_location_from_url, retrieve_skills_from_url, read_file, retrieve_job_title, get_title_patterns
from src.config import *
import re
from selenium.webdriver.support.ui import WebDriverWait

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = "data/uploaded_files"

ALLOWED_EXTENSIONS = ["pdf"]
rand_str = lambda n: ''.join([random.choice(string.ascii_lowercase + string.digits) for _ in range(n)])


@app.route("/")
def home():
    global locations, skills, german_job_titles_pattern, english_job_titles_pattern
    locations = get_location(COUNTRIES, LOCATION_PATH)
    skills = get_skills(SKILL_PATH + os.sep + SKILL_FILE)
    german_job_titles = get_title_patterns(TITLE_PATH + os.sep + GERMAN_TITLE_FILE)
    german_job_titles_pattern = re.compile("(\w*(" + '|'.join(german_job_titles) + "))", re.IGNORECASE)
    english_job_titles = get_title_patterns(TITLE_PATH + os.sep + ENGLISH_TITLE_FILE)
    english_job_titles_pattern = re.compile("\S*(" + '|'.join(english_job_titles) + ")", re.IGNORECASE)
    return render_template("upload.html")


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/file', methods=['POST'])
def file():
    file = request.files['file']
    if file.filename == '':
        flash('No file selected or url provided !')
        return redirect(request.url)
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        extension = filename.split(".")[-1]
        new_name = rand_str(20) + '.' + extension
        saved_file = os.path.join(app.config['UPLOAD_FOLDER'], new_name)
        file.save(saved_file)
        job_ads = read_file(saved_file)
        os.remove(saved_file)
        country, city = retrieve_location_from_url(job_ads, locations)
        extracted_skills = retrieve_skills_from_url(job_ads, skills)
        title = retrieve_job_title(job_ads, german_job_titles_pattern, english_job_titles_pattern)
        return render_template("result.html", country=country, city=city, skills=extracted_skills, title = title)


@app.route("/url", methods=["POST"])
def url():
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    driver = webdriver.Chrome(chrome_options=chrome_options)
    url = request.form['url']
    driver.get(url)
    WebDriverWait(driver, 10000)
    job_ads = driver.find_element_by_xpath("//div/div/div[@class='tem1ContentBlock']").text
    country, city = retrieve_location_from_url(job_ads, locations)
    extracted_skills = retrieve_skills_from_url(job_ads, skills)
    raw_title = driver.find_element_by_xpath("//div[@class='tem1-HdrContainer']/div/h1").text
    title = retrieve_job_title(raw_title, german_job_titles_pattern, english_job_titles_pattern)
    if not len(title):
        title = raw_title
    return render_template("result.html", country=country, city=city, skills=extracted_skills, title = title)


if __name__ == "__main__":
    app.secret_key = os.urandom(12)
    app.run(host='0.0.0.0', port=4555, debug=True)
