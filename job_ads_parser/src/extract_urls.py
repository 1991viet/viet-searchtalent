import argparse
import os
import shutil
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.options import Options

USERNAME = 'nhviet1009@gmail.com'
PASSWORD = 'Tini27061996?'
# USERNAME = 'prabhukumarreddy204@gmail.com'
# PASSWORD = 'prabhu@1992'
# USERNAME = "maja.sommer@vb-business-group.com"
# PASSWORD = "www.aemp3.de"
DATA_PATH = "../data/urls"
code_range = [4962, 5037]


def get_args():
    parser = argparse.ArgumentParser("Extract profile's url from Linkedin")
    parser.add_argument("-d", "--display_hidden", type=bool, default=True)
    args = parser.parse_args()
    return args


def log_in(driver):
    driver.get("https://www.linkedin.com/")
    driver.find_element_by_id('login-email').send_keys(USERNAME)
    driver.find_element_by_id('login-password').send_keys(PASSWORD)
    driver.find_element_by_id('login-submit').click()
    return driver


def main(opt):
    chrome_options = Options()
    if opt.display_hidden:
        chrome_options.add_argument("--headless")
    for code in range(code_range[0], code_range[1] + 1):
        driver = webdriver.Chrome(chrome_options=chrome_options)
        actions = ActionChains(driver)
        driver = log_in(driver)

        WebDriverWait(driver, 2000)
        driver.get(
            "https://www.linkedin.com/search/results/people/?facetGeoRegion=%5B%22de%3A{}%22%5D&origin=FACETED_SEARCH".format(
                code))
        WebDriverWait(driver, 2000)
        location = driver.find_element_by_xpath("//div/div/h3[contains(@class, 'search-s-facet__name')]").text
        f = open("{}/{}.txt".format(DATA_PATH, location), "w")
        print("Retrieving urls from {} ...".format(location))
        stored_urls = []
        while True:
            WebDriverWait(driver, 2000)
            driver.execute_script("window.scrollTo(0, Math.ceil(document.body.scrollHeight/4));")
            WebDriverWait(driver, 2000)
            driver.execute_script("window.scrollTo(0, Math.ceil(document.body.scrollHeight/2));")
            WebDriverWait(driver, 2000)
            driver.execute_script("window.scrollTo(0, Math.ceil(document.body.scrollHeight*3/4));")
            WebDriverWait(driver, 2000)
            candidate_htmls = driver.find_elements_by_xpath("//div[contains(@class,'_info pt3 ')]/a")
            try:
                candidate_urls = [i.get_attribute("href") for i in candidate_htmls]
            except:
                continue

            for url in candidate_urls:
                if "https://www.linkedin.com/search/results/people/?" not in url and url not in stored_urls:
                    print(url)
                    stored_urls.append(url)
                    f.write(url + "\n")
            try:
                try:
                    messaging_boxes = driver.find_elements_by_xpath(
                        "//button[@class='msg-overlay-bubble-header__control js-msg-close']")
                    for box in messaging_boxes:
                        box.click()
                except:
                    pass
                WebDriverWait(driver, 2000)
                next_page = driver.find_element_by_xpath("//div/ol/li/button/div[@class='next-text']")
                WebDriverWait(driver, 2000)
                actions.click(next_page).perform()
            # except StaleElementReferenceException:
            except:
                print("Finish retrieving urls from {}".format(location))
                f.close()
                driver.close()
                code += 1
                break
            # except NoSuchElementException:
            #     print("Error when retrieving urls from {}! Do it again...".format(location))
            #     f.close()
            #     driver.close()
            #     break

    input("Press any key to finish ...")


if __name__ == "__main__":
    opt = get_args()
    # if os.path.isdir(DATA_PATH):
    #     shutil.rmtree(DATA_PATH)
    # os.makedirs(DATA_PATH)
    main(opt)
