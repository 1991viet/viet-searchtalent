import os
import glob
import argparse

DATA_PATH = "../data"


def get_args():
    parser = argparse.ArgumentParser("Split url files into smaller ones")
    parser.add_argument("-b", "--split_size", type=int, default=100)
    args = parser.parse_args()
    return args


def main(opt):
    for read_file in glob.iglob('{}/urls/*.txt'.format(DATA_PATH)):
        if "skills.txt" in read_file:
            continue
        file_index = 0
        count = 0
        w_file = open(read_file.replace("urls", "splitted_urls")[:-4] + "_{}".format(file_index + 1) + ".txt", "w")
        with open(read_file, "r") as f:
            urls = f.readlines()
            for url in urls:
                w_file.write(url)
                count += 1
                if count == opt.split_size:
                    file_index += 1
                    count = 0
                    w_file.close()
                    w_file = open(
                        read_file.replace("urls", "splitted_urls")[:-4] + "_{}".format(file_index + 1) + ".txt", "w")


if __name__ == "__main__":
    opt = get_args()
    main(opt)
