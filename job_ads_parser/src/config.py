LOCATION_PATH = "data/location"
LOCATION_FILE = "World_Cities_Location_table.csv"
SKILL_PATH = "data/skills"
SKILL_FILE = "skills.txt"
COUNTRIES = ["Germany", "United Kingdom", "United States"]
TITLE_PATH = "data/titles"
GERMAN_TITLE_FILE = "German_job_titles.csv"
ENGLISH_TITLE_FILE = "English_job_titles.csv"