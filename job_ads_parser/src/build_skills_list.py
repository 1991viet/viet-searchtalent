import os
import glob
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
import argparse
from selenium.webdriver.chrome.options import Options


USERNAME = "darithomas217@gmail.com"
PASSWORD = "Viet1991"
# USERNAME = "wsqiuning@hotmail.com"
# PASSWORD = "125689yln"
# USERNAME = 'nhviet1009@gmail.com'
# PASSWORD = 'Tini27061996?'
# USERNAME = 'georgc73@gmx.de'
# PASSWORD = 'wimJa55e!'
# USERNAME = 'meinjobprofil@gmx.de'
# PASSWORD = 'google123'
# USERNAME = 'prabhukumarreddy204@gmail.com'
# PASSWORD = 'prabhu@1992'
# USERNAME = 'fredo92neu@gmx.de'
# PASSWORD = 'linked1nm8Spaß'
# USERNAME = "maja.sommer@vb-business-group.com"
# PASSWORD = "www.aemp3.de"
DATA_PATH = "../data"

def get_args():
    parser = argparse.ArgumentParser("Extract profile's url from Linkedin")
    parser.add_argument("-t", "--time", type=int, default=10000)
    parser.add_argument("-d", "--display_hidden", type=bool, default=True)
    args = parser.parse_args()
    return args


def log_in(driver):
    driver.get("https://www.linkedin.com/")
    driver.find_element_by_id('login-email').send_keys(USERNAME)
    driver.find_element_by_id('login-password').send_keys(PASSWORD)
    driver.find_element_by_id('login-submit').click()
    return driver

def main(opt):
    chrome_options = Options()
    if opt.display_hidden:
        chrome_options.add_argument("--headless")
    driver = webdriver.Chrome(chrome_options=chrome_options)
    driver = log_in(driver)
    WebDriverWait(driver, opt.time)
    for read_file in glob.iglob('{}/splitted_urls/*.txt'.format(DATA_PATH)):
        skills = []
        write_file = read_file.replace("splitted_urls", "skills")
        if os.path.isfile(write_file):
            print ("File {} is already processed. Continue with the next file".format(read_file))
            continue
        w_file = open(write_file, "w")
        print ("Opening file {} and writing to file {}.".format(read_file, write_file))
        with open(read_file, "r") as f:
            candidate_urls = f.readlines()
        for result in candidate_urls:
            try:
                driver.get(result[:-1])
                print (driver.current_url)
                WebDriverWait(driver, opt.time)
                actions = ActionChains(driver)
                for i in range(10):
                    driver.execute_script("window.scrollTo(0, Math.ceil(document.body.scrollHeight*{}/10));".format(i+1))
                    WebDriverWait(driver,opt.time)
                    try:
                        messaging_boxes = driver.find_elements_by_xpath(
                            "//button[@class='msg-overlay-bubble-header__control js-msg-close']")
                        for box in messaging_boxes:
                            box.click()
                    except:
                        pass
                    try:
                        show_more = driver.find_element_by_xpath("//div/button[contains(@class, 'pv-profile-section__card-action-bar pv-skills-section__additional-skills artdeco-container-card-action-bar')]/span[2]")
                        actions.click(show_more).perform()
                        break
                    except:
                        continue

                all_skills = driver.find_elements_by_xpath("//li/div/p[contains(@class,'pv-skill-category-entity__name')]")
                print ("There are {} non-unique skills".format(len(all_skills)))
                for skill in all_skills:
                    skl = skill.text.lower()
                    if skl not in skills:
                        skills.append(skl)
                        w_file.write(skl + "\n")
                        print (skl)
            except:
                pass
        w_file.close()
        driver.close()
        driver = webdriver.Chrome(chrome_options=chrome_options)
        driver = log_in(driver)
        WebDriverWait(driver, opt.time)
        # input("Finish")

if __name__ == "__main__":
    opt = get_args()
    main(opt)