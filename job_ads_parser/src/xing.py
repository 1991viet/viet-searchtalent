import os
import glob
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
import argparse
from selenium.webdriver.chrome.options import Options


USERNAME = "christopherbaade@gmail.com"
PASSWORD = "WOL1337le"

def get_args():
    parser = argparse.ArgumentParser("Extract profile's url from Linkedin")
    parser.add_argument("-t", "--time", type=int, default=10000)
    parser.add_argument("-d", "--display_hidden", type=bool, default=False)
    args = parser.parse_args()
    return args


def log_in(driver):
    driver.get("https://login.xing.com/")
    driver.find_element_by_id('login_form_username').send_keys(USERNAME)
    driver.find_element_by_id('login_form_password').send_keys(PASSWORD)
    driver.find_element_by_xpath("//div/button[@class='element-form-button-solid-lime']").click()
    return driver

def main(opt):
    chrome_options = Options()
    if opt.display_hidden:
        chrome_options.add_argument("--headless")
    driver = webdriver.Chrome(chrome_options=chrome_options)
    driver = log_in(driver)
    input("press...")

if __name__ == "__main__":
    opt = get_args()
    main(opt)