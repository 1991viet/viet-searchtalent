import os
import pandas as pd
from src.config import LOCATION_PATH, LOCATION_FILE

df = pd.read_csv(LOCATION_PATH + os.sep + LOCATION_FILE, sep=";", header=None, usecols=[1, 2]).groupby(1)
groups = dict(list(df))
for key, value in groups.items():
    value[2].to_csv(LOCATION_PATH + os.sep + key + ".csv", index=False)

