import os
import pandas as pd
from src.config import COUNTRIES
import re
from docx import Document
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from io import StringIO
import subprocess
from bs4 import BeautifulSoup

def get_location(countries, folder):
    dfs = []
    converter = lambda x: x.lower()
    for country in countries:
        df = pd.read_csv(folder + os.sep + country + ".csv", header = None, converters={0:converter})
        df[1] = country.lower()
        dfs.append(df)
    return pd.concat(dfs)

def get_skills(path):
    converter = lambda x: x.replace('+', '\+').replace('*', '\*').replace('?', '\?').replace('.','\.')
    df = pd.read_csv(path, header=None, sep=";", converters={0:converter}, encoding = "ISO-8859-1")
    return df.values.flatten()

def get_title_patterns(file_path):
    with open(file_path) as f:
        job_titles = [row[:-1].replace("-", "-*") for row in
                      f.readlines()]
    job_titles.sort(key=len, reverse=True)
    return job_titles

def job_title_localization(text):
    pattern = re.compile(
        "(.*\n)?.*((\(* *m *[\/|\|] *w *\)*)|(\(* *w *[\/|\|] *m *\)*)|(\(* *m *[\/|\|] *f *\)*)|(\(* *f *[\/|\|] *m *\)*))", re.IGNORECASE)
    area = pattern.search(text)
    if area:
        return area.group(0).replace("\n", " ").lower().title()
    else:
        return text

def retrieve_job_title(text, german_job_titles_pattern, english_job_titles_pattern):
    job_titles = []
    job_title_area = job_title_localization(text)
    if job_title_area == text:
        job_title_area = job_title_area[:int(len(job_title_area)/2)]
    german_potential_job_title = german_job_titles_pattern.findall(job_title_area)
    if (len(german_potential_job_title) != 0):
        german_potential_job_title = [item[0].lower().title() for item in german_potential_job_title]
        german_potential_job_title = list(set(german_potential_job_title))
        job_titles.extend(german_potential_job_title)

    english_potential_job_title = english_job_titles_pattern.search(job_title_area)
    if (english_potential_job_title):
        english_title = english_potential_job_title.group(0).lower().title()
        if english_title not in german_potential_job_title:
            job_titles.append(english_title)

    return job_titles



def retrieve_location_from_url(text, locations):
    if "Germany" in COUNTRIES:
        text = text.replace('ä', 'ae').replace('ö', 'oe').replace('ü', 'ue').replace('ß','ss')
    cities_list = locations[0].values
    city = re.search("(" + '|'.join(cities_list) + ")", text, re.IGNORECASE)
    if city:
        city = city.group(0)
    else:
        city = ""
    if city:
        try:
            country = locations[locations[0] == city.lower()].values[0][1]
        except:
            country = ""
    else:
        country = ""
    return country.title(), city

def retrieve_skills_from_url(text, skills):
    extracted_skills = re.findall("(\W|^)(" + '|'.join(skills) + ")(\W|$)", text, re.IGNORECASE)
    if len(extracted_skills):
        extracted_skills = [item[1] for item in extracted_skills]
    return list(set(skill.lower() for skill in extracted_skills))


def convertDocxToText(path):
    document = Document(path)
    return "\n".join([para.text for para in document.paragraphs])

def convertPDFToText(path):
    rsrcmgr = PDFResourceManager()
    retstr = StringIO()
    codec = 'utf-8'
    laparams = LAParams()
    device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
    fp = open(path, 'rb')
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    password = ""
    maxpages = 0
    caching = True
    pagenos = set()
    for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password, caching=caching,
                                  check_extractable=True):
        interpreter.process_page(page)
    fp.close()
    device.close()
    string = retstr.getvalue()
    retstr.close()
    return string

def read_file(fileName):
    extension = fileName.split(".")[-1]
    if extension == "txt":
        f = open(fileName, 'r')
        string = f.read()
        f.close()
        return string
    elif extension == "doc":
        return subprocess.Popen(['antiword', fileName], stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()[
                   0]
    elif extension == "docx":
        try:
            return convertDocxToText(fileName)
        except:
            return ""
    elif extension == "pdf":
        try:
            return convertPDFToText(fileName)
        except:
            return ""
    else:
        print ('Unsupported format')
        return ""

if __name__ == "__main__":
    file_ = "data/exb_senior_javadeveloper_DE.pdf"
    text = read_file(file_)
    print (text)


