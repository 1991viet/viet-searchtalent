from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
import os
from selenium.webdriver.common.action_chains import ActionChains
import glob
import argparse
from selenium.webdriver.chrome.options import Options

DATA_PATH = "../data"

def main():
    unique_skills = []
    w_file = open(os.path.join(DATA_PATH, "skills", "skills.txt"), "w")
    for read_file in glob.iglob('{}/skills/*.txt'.format(DATA_PATH)):
        if "skills.txt" in read_file:
            continue
        print ("Reading file {} ...".format(read_file))
        with open(read_file, "r") as f:
            skills = f.readlines()
            for skill in skills:
                skill = skill.replace(";", ",")
                if skill not in unique_skills:
                    print (skill)
                    unique_skills.append(skill)
                    w_file.write(skill)
    w_file.close()



if __name__ == "__main__":
    main()