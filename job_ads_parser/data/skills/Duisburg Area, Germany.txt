enterprise information management
enterprise information architecture
product management
etl
data integration
business intelligence
big data
python
java
abap
php
hadoop
software-as-a-service (saas)
master data management (mdm)
cloud-based mdm
veeva network
veeva crm
customer data integration (cdi)
data quality
efpia / transparency
life science industry knowledge
data quality reporting / cockpits
sap hana
sap data services
sap information steward
dqm for sap solutions
sap netweaver mdm
sap master data governance (mdg)
hana sql script
mysql / ms sql / oracle / db2
web services / soa
linux / unix shell scripting
databases
data warehousing
project management
it outsourcing
software project management
it management
it operations
software development
lean management
change management
it service management
sql
oracle
management
crm databases
crm integration
projektmanagement
datenbanken
datenlager
business-intelligence
unternehmensführung
softwareentwicklung
anforderungsanalyse
veränderungsmanagement
c++
algorithms
object oriented design
microsoft office
latex
javascript
linux
jquery
jsp
html
pascal
ibm mainframe
db2
z/os
itil
mainframe
prince2
incident management
ict
process management
banking
disaster recovery
enterprise architecture
outsourcing
performance tuning
user acceptance testing
data modeling
business continuity
consultancy
jcl
operating systems
cobol
aix
websphere
unix
websphere application server
sharepoint
tso
vsam
ispf
mvs
cics
db2/sql
rexx
fileaid
os/390
endeavor
ims db/dc
websphere mq
capacity planning
racf
application lifecycle management
easytrieve
endevor
service management
it-projektmanagement
it-management
it-consulting
dns
voice over ip
webdesign
responsive webdesign
vmware
active directory
microsoft windows
microsoft exchange
hyper-v
microsoft excel
it-systemadministration
strategische planung
fehlerbehebung
systemadministration
virtualisierung
kundendienst
technisches verständnis
käufmännische kenntnisse
technischer vertrieb
swyx
nfon
grafikdesign
rechnernetze
programmierung
fernwartungssupport
systemintegratoren
it security policies & procedures
it-strategie
it-operationen
it-architektur
anwenderschulungen
human resources
coaching
personal- und organisationsentwicklung
recruiting
employer branding
onboarding
strategic planning
performance management
continuous improvement
tpm
fmcg
gmp
iso 14001
iso 9000
haccp
social media
sap hcm
sap hr
leadership development
führungskräftetraining
hr project management
burnout-prävention
konfliktmanagement
personalführung
personalbeschaffung
personalrichtlinien
personalwesen
persönlichkeitsentwicklung
strategische personalplanung
autocad
catia
automotive engineering
simulations
automotive
ansys
matlab
simulink
microsoft word
microsoft powerpoint
c
programmable logic controller (plc)
solidworks
siemens nx
labview
public speaking
fem analysis
microcontroller programing
account management
security
direct sales
solution selling
channel partners
key account management
cloud computing
storage
professional services
business development
sales
sales process
international business
pre-sales
managed services
strategy
network security
unified communications
data center
virtualization
information security
go-to-market strategy
customer experience
channel sales
enterprise software
saas
salesforce.com
partner management
strategic partnerships
business networking
lead generation
professional networking
training
business alliances
customer service
customer engagement
strategic alliances
great personality
cross-cultural competence
new business development
people-oriented
great motivator
security+
selling
great people skills
direct touch
great results
marketing
social media marketing
eventplanung
organisiertes arbeiten
business analytics
export
teamwork
prozessoptimierung
kreative lösungen
finanzanalyse
logistikmanagement
zollabwicklung
aufgeschlossenheit
verkaufsunterstützung
key account
sponsoring
trade marketing
eventmanagement
sponsoringaktivierung
brandmarketing
category-management im einzelhandel
magazine
public relations
science outreach
research
magazines
lecturing
qualitative research
teaching
public understanding of science
science communication
technology journalism
popular science
science journalism
science policy
digital journalism
journalism education
citizen science
open innovation
wissenschaftskommunikation
unterrichten
forschung
wissenschaft
marketingkommunikation
journalismus
strategisches management
lektorat
strategische kommunikation
innovationskommunikation
qualitative forschung
unternehmensstrategie
unternehmenskommunikation
vorlesungen
wissenschaftsmanagement
digital marketing
sem
web project management
e-commerce
crm
product development
web development
marketing strategy
online advertising
concept development
agile project management
online marketing analysis
mobile internet
digital transformation
sitecore
digital architecture
customer experience transformation
customer experience consulting
international trade
import
sales management
international sales
purchasing
negotiation
meat
beef
poultry
seafood
frozen food
analysis
accounting
analytical skills
financial accounting
financial analysis
financial reporting
auditing
finance
account reconciliation
accounts payable
accounts receivable
corporate finance
internal controls
project planning
powerpoint
team management
team leadership
team building
leadership
german
business english
business strategy
us gaap
sap
siebel
blockchain
cultural awareness
english
chinese
international finance
chinese culture
financial controlling
gesprächsführung
automobilindustrie
internationaler vertrieb
neugeschäftsentwicklung
vertriebsleitung
vertragsverhandlungen
ingenieurwissenschaften
kundenzufriedenheit
vertrieb
market research
data analysis
program development
produktentwicklung
qualitätsmanagement
schlanke produktion
geschäftsführung
verkaufsleitung
hernieuwbare energy
bedrijfsstrategie
energiebesparing
zonne-energie
techniek
duurzaamheid
contractonderhandeling
energieopwekking
dell
account-management
prozessverbesserung
lösungsverkauf
vertrieb von ersatzteilen nahmhafter hersteller wie hp
ibm / lenovo
asus
acer
apple
samsung
optionen
einkauf
verkauf
offene und ehrliche geschäftsbeziehung
kundenbetreuung
kompetenz und erfahrung im ersatzteilmanagement und after sales- services
hp drucker
marketing und vertrieb
unternehmensanalyse
technical expertise
strategic thinking
multicultural team member
hotels
event management
budgets
tourismus
marketingstrategie
hotel- und gaststättengewerbe
teamentwicklung
pricing - yieldmanagement
teamleitung
hoteleinkauf - contracting
katalogplanung
produktmanagement
kalkulation
medical case management
differetial diagnosis
clinical pharmacology
clinical research
internal medicine
general surgery
medical group management
book writing
leergierig
receptie
administratief vaardigheden
enthousiast
salestopper
zelfstandig
flexibel
ambitieus
doelgericht
targetgericht
marketing communications
corporate communications
management consulting
german law
unternehmensberatung
management, sales, (online)marketing, event management, nl-d business
deutsch
nederlands-duitse samenwerking
kennis van de duitse markt
vorträge
gesundheitspflege
mechanical engineering
cad
engineering
manufacturing
microsoft project
unternehmensplanung
fertigung
prozesssteuerung
creo parametric
product design
finite element analysis
strategic management
computer-aided design (cad)
condition monitoring
fundraising
englisch
akquise
geometric dimensioning & tolerancing
sheet metal
plastics
powertrain
operations management
project coordination
ptc creo
nx unigraphics
biw
automotive interiors
exterior design
automotive design
hypermesh
business planning
business plan
strategia d'impresa
strategia di marketing
consulenza manageriale
pianificazione strategica
analisi
petrochemical
project control
contract management
project engineering
project estimation
process scheduler
fabrikanlagen
geschäftsprozesse
politik
zeitmanagement
soziale medien
interviews
performance appraisal
job descriptions
hr policies
employment law
payroll
screening
new hire orientations
strategic hr
hiring
workforce planning
recruitment advertising
deferred compensation
technical recruiting
job evaluation
hr consulting
compensation & benefit
screening resumes
executive search
internet recruiting
benefits negotiation
benefits administration
succession planning
employee benefits
hris
employee relations
personnel management
talent management
employee training
labor relations
organizational development
employee engagement
sourcing
soft skills
job coaching
exit interviews
job analysis
salary
competency based interviewing
competency based assessment
talent acquisition
recruitments
dairy
excel
hp
spanisch
italienisch
trade
milcherzeugnisse
food industrie
lebensmittelindustrie
supply-chain-management
netzwerke
organizational leadership
advertising
quality management
dutch
turkish
hospitality management
hotelgewerbe
gastgewerbe
business-to-business
key account development
b2b
sales operations
key-account-management
vertriebsorganisation
kontinuierliche verbesserung
مدرب العلوم الإدارية
مدرب متخصص باعداد المدربين الدوليين
مدرب المهارات القيادية
مؤلف لعدد من المحاضرات والكتب بعلم التنمية البشرية
مؤسسة المدرسة التمثيلية الواقعية
international relations
final destinations
goods exchange
cbd oil manufactoring and trade
excise and duties
supply chain management
retail
einzelhandel
teamführung
bestandsverwaltung
budgetierung
production planning
proe
machinetools
factory design
engineering design
six sigma
failure mode and effects analysis (fmea)
dmaic
production part approval process (ppap)
advanced product quality planning (apqp)
erp
software implementation
enterprise resource planning (erp)
start-ups
business analysis
business process improvement
customer relationship management (crm)
navision
microsoft dynamics
interim management
general management
consultants
goal oriented
projektabwicklung
technisches training
abteilungsübergreifende teamführung
elektronik
kundendienst-management
research writing
healthcare
nonprofits
event planning
communication
debate
afghanistan
internationale relaties
leger
militaire operaties
overheid
defensie
bedrijfsprocesplanning
aanvoeren
landmacht
veranderingsmanagement
nederlands
strategische communicatie
didactiek
sap erp
autodesk inventor
mode
sap business one
cisco certified
ccna
energie
erneuerbare energien
energieeffizienz
cad/cam
flow simulation
cnc machine
machine operation
aerodynamics
outlook
merchandising
textilien
wirtschaftsrecht
energierecht
unternehmensführung und -kontrolle
devisenhandel
strategische führung
5s
lieferkette
ausbilder für ehrenamtliche hospizhelfer nach dem celler modell
purchase
manufacturing technology
medizin
gesundheitsmanagement
freight
internationale logistik
produktionsmanagement
logistik
transportlogistik
immobilien
wohnimmobilien und kapitalanlagen
microsoft outlook
strukturierte finanzierung
controlling
ausbildung von kaufleuten
motivierende gesprächsführung
validierung
umgang mit dementiell beeinträchtigte personen
grenzüberschreitende beratung deutschland-niederlande
steuerrecht
gesellschaftsrecht
arbeitsrecht
servicedesk
projektplanung
projektierung
elektriker
maschinenbau
servicemanagement
logistics
verkehr
beschaffung
optimierung von geschäftsabläufen
lagerwesen
vaskulär
produkteinführung
marktentwicklung
kardiologie
krankenhäuser
medizinprodukte
european union
politikfeldanalyse
internationale beziehungen
europäische union
öffentlichkeitsarbeit
geschäftsentwicklung
sap mm
consulting
software
integration
sd
sap sd
sap netweaver
sap abap
lotus notes
computer hardware
hardware
sap retail
sap ewm
materialwirtschaft
sap logistics execution
sap logistics
sap pp
sap lo
microsoft servers
produktionsplanung
lagerverwaltung
support
horeca
hospitality
catering
food & beverage
tourism
food
restaurants
zoo
b2c
horeca, event management,hospitality,.
trading
hedging
derivatives
commodity markets
agribusiness
risk management
forex
strategic consulting
client relations
equity indices
energy markets
cross-functional coordination
stock trading
gold trading
derivate
außenhandel
asic
soc
vhdl
debugging
verilog
perl
upf
static timing analysis
place & route
design for test
ieee
low power
synopsys
halbleiter
eingebettete systeme
integrierte schaltkreise
p1801
job offer
formulation
design of experiments
metalworking fluids
chemie
polymere
chemietechnik
organische chemie
forschung und entwicklung
surfactants - synthesis & application
beschichtungen
materialwissenschaft und werkstofftechnik
ibm
xseries
server
ibm storwize
cloud-computing
speicher
schreiben
soziale netzwerke
hochschulwesen
organisationsentwicklung
personalentwicklung
real estate
corporate law
entrepreneurship
problem solving
presenter
niederländisch
mentaliteitsverschillen in de nederlands-duitse business
duits belastingrecht
duits vennootschapsrecht
duitsland
vastgoed
interim-management
unternehmertum
elektrogesetz
verpackungsverordnung
mathematics
aviation
object-oriented programming (oop)
people skills
conflict management
manufacturing drawings
manufacturing modeling
process optimization
food safety
process engineering
lebensmittelverarbeitung
nahrungsmittel
lebensmittelsicherheit
factory
interne kommunikation
pro e
brazing
metallization
optical microscopy
cmm
fixture design
chirurgie
klinische forschung
cisco technologies
servers
resellers
computer-hardware
wiederverkäufer
condition based maintenance
trainer
condition based monitoring
personalmanagement
wartungsmanagement
moderne instandhaltungssysteme
zustandsorientierte beobachtung
service vertrieb
zertifizierter auditor für qualitätsmanagement
professionelle optimierung von maschinen und anlagen
smed - workshops
personalentwicklung im service
aufbau und entwicklung von tochtergesellschaften weltweit
commercial aviation
airlines
flights
hörfunk
redaktion
storytelling
radiowerbung
online-journalismus
fotokünstler aus leidenschaft
programmberater
mediencoach - berater - präsentation
import/export
inside sales
mechatronics
program management
bookkeeping
sap accounting
product marketing
market development
competitive analysis
product launch
cross-functional team leadership
xrf
cnc programming
fluid mechanics
hperworks
leiderschap
spreken in het openbaar
sociale media
statistical data analysis
economics
statistics
investment banking
financial risk
financial markets
stata
spss
biochemistry
italian
molecular biology
life sciences
synthetic biology
piano playing
molecular genetics
prognosen
beschaffungsmanagement
materials science
structural analysis
multibody dynamics
vibration
tribology
material selection
sketchup
adobe photoshop
coreldraw
gnu octave
blender
spaceclaim
finanzbuchhaltung
rechnungswesen
finanzberichterstattung
interne kontrolle
wirtschaftsprüfung
kreditorenbuchhaltung
telecommunications
mobile devices
produktmarketing
mobile geräte
telekommunikation
unterhaltungselektronik
mobile kommunikation
funkverkehr
strategische partnerschaften
go-to-market
mobiltechnologie
vertriebspartner
cisco
selbstvertrauen
teamfähigkeit
flexibilität
3d studio max
sketching
graphic design
interior design
rendering
painting
architectural design
landscape design
revit
photoshop
garden design
logistics management
transportation management
international logistics
freight forwarding
trucking
transportation planning
process improvement
reverse logistics
container
ltl
truckload
transportation
third-party logistics (3pl)
warehouse management
contract negotiation
forwarding
logistics engineering
roadtransport
projects
medical devices
pro-e
nondestructive testing (ndt)
cad/cam software
administration
microsoft access
adobe premiere pro
intercultural communication
servicenow
abbyy
ui automation
packaging innovation
packaging development
packaging materials
industrial engineering
maintenance management
r&d
packaging processes
datev
six sigma, lean manufacturing, gmp, fda, anvisa, project management
product- and process optimization, s&op-process
kommunikation
policy
government
politics
national security
regierung und behörden
nationale sicherheit
militär
befehlsgewalt
verteidigung
katastrophenschutz
verwaltungsrecht
kommunalrecht
öffentliche ordnung
krisenmanagment
führungslehre
betriebsplanung
zivil-militärische zusammenarbeit
ausländerrecht
öffentliche sicherheit
öffentliche verwaltung
asylrecht
aufenthaltsrecht
bevölkerungsschutz
stabslehre
politische kampagnen
krisenkommunikation
notfallplanung
maschinen- und anlagenbau
verhandlungen
multi-projectmanagement
model united nations
ug nx
autodesk moldflow
sap erp customisation
c#
oop
agile methodologies
user experience design
.net
uml
wpf
softwaredesign
softwaretechnik
agile methoden
software-projektleitung
instrumentation
sap crm
strategieentwicklung
problemlösung
organisationsgestaltung
technical documentation
3d modeling
renewable energy
solar energy
energy
research and development (r&d)
solid edge
adobe acrobat
ees
bill of material creation
field installation
ptc pro/engineer
power transmision
lean manufacturing
kaizen
field service
spc
fmea
customer service management
global r&d
technology development
joint ventures
cost reduction strategies
industry 4.0
technical product sales
r
türkische außenpolitik
evaluierung
werving
organisatieontwikkeling
engels
einkaufsprozesse
gastronomiemangement
veranstaltungsmanagement
sicherheitsglas
web engineering
.net framework
software engineering
documentation
tdd
scrum
kanban
spring framework
angularjs
bootstrap
sass
ms sql
oracle sql
mysql
sqlite
junit
html5
json
xml
entity framework
jpa
hibernate
git
jenkins
rest
visual studio
android
presentation
css3
moderation
play framework
evaluation
telerik web controls
nunit
selenium-tests
apache jmeter
lead development
customer contact
rhetoric and communication
mobile engineering
android studio
software architecture
emergency medicine
medicine
medical education
hospitals
healthcare management
public health
intubation
sonographie
pleurapunktion
pathology
warehousing
managerial finance
turn around management
electronics
pcb design
embedded systems
digital circuit design
wireless communications systems
observational astronomy
astrophysics
marketingmanagement
penetration testing
ethical hacker
network administration
data mining
firewalls
database administration
android development
windows server
vmware infrastructure
microsoft sql server
ajax
windows
time management
endpoint security
enterprise network security
networking
unix shell scripting
microsoft operating systems
backup solutions
kommunikationsfähigkeit (z.b. erfahrung mit moderation)
organisation (z.b. probennahmen / events / ...)
nmr
analytisches denken
analytische chemie
massenspektrometrie
datenanalyse
gaschromatographie
spektroskopie
isotopologue profiling
construction engineering
sustainability
systems engineering
technical drawing
ms project
presales
citrix
vdi
rechenzentren
polymers
rohstoffe
kosmetikindustrie
military
military operations
air force
military aviation
mission commander
air operations
international experience
planning
solidworks 3d-cad
autodesk plant 3d
innovation
ce marking
fda
regulatory affairs
quality assurance
iso 13485
testing
quality system
regulatory requirements
staff development
iec 60601
product certification
standards compliance
iso 14971
21 cfr
medical device directive
technical files
medical device r&d
test protocols
regulatory science
global regulatory compliance
regulatory standards
product safety
cmdcas
mdr
regulatory approvals
class iii medical devices
regulatory strategy development
iec 62304
post market surveillance
usability engineering
ivdd
raps
surgical navigation
business process design
business transformation
business modeling
multi-unit retail management
toner cartridges
toner
laser printers
printers
print management
wide format printing
web design
google adwords
cms
photocopier
ricoh
document imaging
inkjet
office equipment
scanners
xerox printers
ink cartridges
canon
plotters
supplies
operative gynaecology, gynaecologic oncology, urogynaecology
showbiz
music industry
music
singing
entertainment
band
orchestral music
music education
styling
live-events
vocal
big band
musicals
songs
studiosinger
lyricist
stand up
ideenentwicklung
eventconcepte
event-organisation
gaming industry
online gaming
security solutions
alarm systems
video surveillance
css
prezi
cadence virtuoso
ocean
history
pop culture
academic writing
editing
visual arts
translation
higher education
literature
university teaching
american literature
transatlantic relations
cultural studies
ethnicity
african american studies
theory
cultural transfer
books
sap supply chain
sap supply chain management
online-marketing
kommunikationsberatung
public policy
policy analysis
international development
stakeholder-management
wirtschaftswissenschaften
spectroscopy
automation
materials
microsoft technologies
microsoft office sharepoint server
solution architecture
software design
web services
architecture
soa
information technology
business process
asp.net mvc
office 365
sql server reporting services (ssrs)
microsoft products
sharepoint server
lync
lync server 2010
archimate
lohnabrechnung
ausbilder
jd edwards
packaging
brand development
design strategy
brand architecture
creative strategy
integrated marketing
art direction
sustainable design
consumer products
fast-moving consumer goods (fmcg)
packaging design
design thinking
strategic design
brand design
creative direction
contact centers
call center
bpo
call centers
workforce management
telemarketing
offshoring
inbound marketing
decision-making in teams
leadership in change management processes
coaching the sales process (skills for sales directors)
konsumgüter
warenrotation
taekwondo
simscale
c and c++ programming
class diagrams
activity diagrams
sequence diagrams
matlab simulink and programming
unified
use case diagram
state diagram
supply chain optimization
supply chain
supply chain operations
english as a second language (esl)
business development consultancy
consumer electronics
business objects
mac os
as400 system
eclipse
programming
mathematical modeling
robotics
trade secrets
microcontrollers
test automation
plc
pspice
arduino
cnc
unified modeling language (uml)
autocad plant 3d
atmel avr
eagle pcb
control engineering
microelectronics
plc ladder logic
digital imaging
digital photography
photography
online marketing
trade shows
social networking
fotografie
digitalfotografie
languages
modeling
cfd
computational fluid dynamics
heat transfer
computational fluid dynamics (cfd)
thermal analysis
stress analysis
fluid dynamics
euv technology
energy engineering
design optimisation
project leadership
digital signage
digital video
streaming media
multimedia
video
digital signal processors
lighting
live sound
sound
system design
audio engineering
av
video conferencing
av integration
videotechnik
audiovision
videokonferenzen
intellectual property
open source licensing
validation
pharmaceutical industry
change control
technology transfer
sop
biotechnology
capa
21 cfr part 11
v&v
biopharmaceuticals
pharmaceutics
vaccines
quality control
lifesciences
quality auditing
glp
process simulation
lims
aseptic processing
gamp
gxp
cleaning validation
computer system validation
französisch
customer support
außendienst
technisches und analytisches verständnis
reisebereitschaft
staalich geprüfter techniker elektrotechnik
mammographie
digital x-ray imaging
chemical engineering
hazop
commissioning
hazop study
process control
pumps
dcs
procurement
refinery
epc
aspen hysys
tank farms
process piping design
thermal insulation
comos
wincc
p
tax
financial advising
steuerberatung
programmmanagement
fusionen und übernahmen
transferpricing
vermögensplanung
structuring of domestic and foreign investment
international inheritance and gift tax legislation
real estate transfer tax
emloyee assignment
real estates
network, regulatory and competition economics
statistics, econometrics, empirical methods
teaching and research
market analysis
transport and logistics
energy and water markets
telecommunications and postal services
costing and pricing
microeconomics
mergers & acquisitions
unternehmensinformatik
partnerschaften
requirements analysis
it-service-managment
cost management
microcontroller programming
alles
vertriebsaktivitäten
vertriebsprozesse
embedded c
plc programming
internet of things
mqtt
micro python
smarteam
vba
ingénierie
conception de produit
amélioration continue
anglais
français
allemand a2
steel
heat treatment
metalworking
metallurgy
welding
manufacturing engineering
yoga
failure analysis
composites
induction heating
material characterisation
mechanical testing
material properties
solid modeling
metallography
long distance running
trekking
cross-cultural teams
mechanical testing of materials
mechanical properties
iso
injection molding
mold design
optics
pro engineer
hvac
building automation
